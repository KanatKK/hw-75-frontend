import {ADD_MESSAGE, ADD_PASSWORD, GET_DECODED_MESSAGE, GET_ENCODED_MESSAGE,} from "./actionTypes";
import axios from 'axios';

export const addMessage = value => {
    return {type: ADD_MESSAGE, value};
};

export const addPassword = value => {
    return {type: ADD_PASSWORD, value};
};

export const getEncodedMessage = value => {
    return {type: GET_ENCODED_MESSAGE, value};
};

export const getDecodedMessage = value => {
    return {type: GET_DECODED_MESSAGE, value};
};

export const cipherMessage = (value) => {
    return async dispatch => {
        return axios.post('http://localhost:8000/encode', value).then(response => {
            dispatch(getEncodedMessage(response.data));
        });
    };
};

export const decipherMessage = (value) => {
    return async dispatch => {
        return axios.post('http://localhost:8000/decode', value).then(response => {
            dispatch(getDecodedMessage(response.data));
        });
    };
};