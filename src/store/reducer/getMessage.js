import {GET_DECODED_MESSAGE, GET_ENCODED_MESSAGE} from "../actionTypes";

const initialState = {
    encodedMessage: {encoded: ''},
    decodedMessage: {decoded: ''},
};

const getMessage = (state = initialState, action) => {
    switch (action.type) {
        case GET_ENCODED_MESSAGE:
            return {...state, encodedMessage: action.value};
        case GET_DECODED_MESSAGE:
            return {...state, decodedMessage: action.value};
        default:
            return state;
    }
};

export default getMessage