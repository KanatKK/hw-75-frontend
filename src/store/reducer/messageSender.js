import {ADD_MESSAGE, ADD_PASSWORD} from "../actionTypes";

const initialState = {
    message: '',
    password: '',
};

const messageSender = (state=initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE:
            return {...state, message: action.value};
        case ADD_PASSWORD:
            return {...state, password: action.value};
        default:
            return state;
    }
};

export default messageSender;