import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";
import {Provider} from "react-redux";
import messageSender from "./store/reducer/messageSender";
import './index.css';
import App from './containers/app/App';
import * as serviceWorker from './serviceWorker';
import getMessage from "./store/reducer/getMessage";

const rootReducers = combineReducers({
    send: messageSender,
    get: getMessage,
});
const store = createStore(rootReducers, applyMiddleware(thunkMiddleware));

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
