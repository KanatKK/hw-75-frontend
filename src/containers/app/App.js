import React from 'react';
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {
    addMessage,
    addPassword,
    cipherMessage,
    decipherMessage,
} from "../../store/actions";

const App = () => {
    const dispatch = useDispatch();
    const sendState = useSelector(state => state.send);
    const password = useSelector(state => state.send.password);

    const encodedMsg = useSelector(state => state.get.encodedMessage);
    const decodedMsg = useSelector(state => state.get.decodedMessage);

    console.log(password.length)

    const addMessageHandler = event => {
        dispatch(addMessage(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const cipherMessageHandler = async () => {
        await dispatch(cipherMessage(sendState));
    };

    const decipherMessageHandler = async () => {
        await dispatch(decipherMessage(sendState));
    };

    const disabled = password.length === 0

  return (
      <div className="container">
          <div className="block decoded">
              <p>Decoded message</p>
              <textarea defaultValue={decodedMsg.decoded} onChange={addMessageHandler}/>
          </div>
          <div className="block password">
              <p>Password</p>
              <input value={password} onChange={addPasswordHandler} type="text"/>
              <div className="topBtn">
                  <button disabled={disabled} onClick={decipherMessageHandler} type="button">&uarr;</button>
              </div>
              <div className="botBtn">
                  <button disabled={disabled} onClick={cipherMessageHandler} type="button">&darr;</button>
              </div>
          </div>
          <div className="block encoded">
              <p>Encoded message</p>
              <textarea defaultValue={encodedMsg.encoded} onChange={addMessageHandler}/>
          </div>
      </div>
  );
};

export default App;
